function generate() {
    const alfabeto = ['a', 'b'];
    const longitud = parseInt(document.getElementById('longitud').value);
    const outputDiv = document.getElementById('output');
    outputDiv.innerHTML = '';

    outputDiv.innerHTML += 'Vacio<br>';

    for (let j = 1; j <= longitud; j++) {
        kleeneDesarrollador(alfabeto, new Array(j), 0, outputDiv);
    }
}

function kleeneDesarrollador(alfabeto, cadena, comparador, outputDiv) {
    if (comparador == cadena.length) {
        outputDiv.innerHTML += cadena.join('') + '<br>';
        return;
    }

    for (let i = 0; i < alfabeto.length; i++) {
        cadena[comparador] = alfabeto[i];
        kleeneDesarrollador(alfabeto, cadena, comparador + 1, outputDiv);
    }
}
